package com.skills421.drools;

import com.skills421.drools.controller.RuleRunner;
import com.skills421.drools.exceptions.RuleRunnerException;
import com.skills421.drools.facts.Person;
import com.skills421.drools.facts.RuleResponse;

public class Main
{

    public static void main(String[] args) throws RuleRunnerException
    {
        RuleRunner runner = new RuleRunner();

        RuleResponse response = runner.insertFact(new Person("Jon Doe",21)).runRules("people-rules");

        System.out.println(response.getMessage());
    }

}