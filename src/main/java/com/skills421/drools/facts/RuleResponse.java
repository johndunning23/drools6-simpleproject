package com.skills421.drools.facts;

public class RuleResponse implements Fact
{
    private String message;

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }
}
