package com.skills421.drools.controller;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.skills421.drools.exceptions.RuleRunnerException;
import com.skills421.drools.facts.Fact;
import com.skills421.drools.facts.RuleResponse;

public class RuleRunner
{
    private List<Fact> facts = new ArrayList<>();

    public RuleRunner()
    {
    }

    public RuleRunner insertFact(Fact fact)
    {
        facts.add(fact);

        return this;
    }

    public RuleResponse runRules(String ruleset) throws RuleRunnerException
    {
        RuleResponse response = null;

        try
        {
            KieServices ks = KieServices.Factory.get();
            KieContainer kContainer = ks.getKieClasspathContainer();
            KieSession kSession = kContainer.newKieSession(ruleset);

            for (Fact fact : facts)
            {
                kSession.insert(fact);
            }

            kSession.fireAllRules();

            for (Object fact : kSession.getObjects())
            {
                if (fact instanceof RuleResponse)
                {
                    response = (RuleResponse) fact;
                }
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return response;
    }
}
